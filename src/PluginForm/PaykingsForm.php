<?php

namespace Drupal\commerce_paykings\PluginForm;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\serialization\Normalizer\FieldableEntityNormalizerTrait;

/**
 * Paykings Offsite form. Currently not used.
 */
class PaykingsForm extends PaymentMethodAddForm {
  use FieldableEntityNormalizerTrait;
  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {

    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $this->entity;

    $form['payment_details'] = [
      '#parents' => array_merge($form['#parents'], ['payment_details']),
      '#type' => 'container',
      '#payment_method_type' => $payment_method->bundle(),
    ];

    if ($payment_method->bundle() == 'credit_card') {
      $form['payment_details'] = $this->buildCreditCardForm(
        $form['payment_details'],
        $form_state
      );
    }

    // Move the billing information below the payment details.
    if (isset($form['billing_information'])) {
      $form['billing_information']['#weight'] = 10;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    // The JS library performs its own validation.
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\commerce_payment\Exception\DeclineException
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $values = (array) $form_state->getValue(
      $form['payment_details']['#parents']
    );
    $this->entity->paykings_token = $values['payment_credit_card_token'];

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface
     * $payment_method
     */
    $payment_method = $this->entity;
    $payment_method->setBillingProfile(
      $form['billing_information']['#profile']
    );

    $values =& $form_state->getValue($form['#parents']);

    /** @var SupportsStoredPaymentMethodsInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $this->plugin;

    // The payment method form is customer facing. For security reasons
    // the returned errors need to be more generic.
    try {
      $payment_gateway_plugin->createPaymentMethod($payment_method,
        $values['payment_details']);
    }
    catch (DeclineException $e) {
      $this->getLogger('commerce_paykings')->warning($e->getMessage());
      throw new DeclineException(
        'We encountered an error processing your payment method. Please verify
        your details and try again.'
      );
    }
    catch (PaymentGatewayException $e) {
      $this->getLogger('commerce_paykings')->error($e->getMessage());
      throw new PaymentGatewayException(
        'We encountered an unexpected error processing your payment method.
        Please try again later.'
      );
    }
  }

  /**
   * Build PayKings form.
   *
   * @param array $element
   *   Form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Form element.
   */
  public function buildPayKingsForm(
    array $element,
    FormStateInterface $form_state
  ) {

    $element['payment_credit_card'] = [
      '#markup' => '<div id="paykings-credit-card"></div>'
    ];

    $values =& $form_state->getUserInput();

    if (!empty($values['singleUseTokenId'])) {
      $element['payment_credit_card_token'] = [
        '#type' => 'hidden',
        '#value' => $values['singleUseTokenId'],
      ];
    }

    return $element;
  }

}
