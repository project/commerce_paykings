<?php

namespace Drupal\commerce_paykings\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the PayKings payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "paykings",
 *   label = @Translation("Credit card"),
 *   create_label = @Translation("Credit card")
 * )
 */
class PayKings extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Credit card (authorised)');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['paykings_token'] = BundleFieldDefinition::create('string')
      ->setLabel(t('PayKings Token'))
      ->setDescription(t('The PayKings token associated with the credit card.'))
      ->setRequired(TRUE);

    return $fields;
  }

}
