<?php

namespace Drupal\commerce_paykings\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the PayKings payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "paykings_method",
 *   label = @Translation("Paykings Credit card"),
 *   create_label = @Translation("Paykings Credit card")
 * )
 */
class PayKingsPaymentMethod extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Paykings (Credit Card)');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return parent::buildFieldDefinitions();
  }

}
