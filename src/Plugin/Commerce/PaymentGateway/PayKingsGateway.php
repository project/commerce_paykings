<?php

namespace Drupal\commerce_paykings\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_paykings\Client\PayKingsClientInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_paykings\Exception\PayKingsClientException;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the PayKings Payment Gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paykings_gateway",
 *   label = "PayKings (Credit Card)",
 *   display_label = "PayKings",
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "discover", "mastercard", "visa"
 *   },
 * )
 */
class PayKingsGateway extends OnsitePaymentGatewayBase {
  use LoggerChannelTrait;
  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * The PayKings REST API Client.
   *
   * @var \Drupal\commerce_paykings\Client\PayKingsClientInterface
   */
  private $payKingsClient;

  /**
   * The Drupal State Service.
   *
   * @var \Drupal\Core\State\State
   */
  private $state;

  /**
   * Constructs a new PayKings object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_paykings\Client\PayKingsClientInterface $payKingsClient
   *   The PayKings REST API Client.
   * @param \Drupal\Core\State\State $payKingsClient
   *   The Drupal State Service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    PayKingsClientInterface $payKingsClient,
    State $state
  ) {

    // Parent construct.
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    // Class required properties.
    $this->payKingsClient = $payKingsClient;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_paykings.paykings_client'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'secret_key' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Paykings Secret Key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values =& $form_state->getValue($form['#parents']);
      $this->configuration['secret_key'] = $values['secret_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    if ($payment->getState()->value !== 'new') {
      throw new \InvalidArgumentException(
        'The provided payment is in an invalid state.'
      );
    }

    /**
     * @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
     */
    $payment_method = $payment->getPaymentMethod();
    if ($payment_method === NULL) {
      throw new \InvalidArgumentException(
        'The provided payment has no payment method referenced.'
      );
    }

    /**
     * @var \Drupal\commerce_order\Entity\OrderInterface $order
     */
    $order = $payment->getOrder();
    // Make a request to PayKings.
    try {
      $result = $this->payKingsClient->doRequest($payment, $this->configuration);
    }
    catch (PayKingsClientException $e) {
      $this->deletePayment($payment, $order);
      $this->getLogger('commerce_paykings')->warning($e->getMessage());
      throw new HardDeclineException('The payment request failed.', 0, $e);
    }

    // If the payment is not approved.
    if ($result['responsetext'] !== 'SUCCESS' && $result['response'] != 1) {
      $this->deletePayment($payment, $order);
      $errorMessage = $result['responsetext'] . ': ' . $result['responsetext'];

      // Log and throw the error.
      $this->getLogger('commerce_paykings')->error($errorMessage);
      throw new HardDeclineException('
        The provided payment for Paykings has been declined!'
      );
    }

    // Update the local payment entity.
    $request_time = $this->time->getRequestTime();
    $payment->state = $capture ? 'completed' : 'authorization';
    $payment->setRemoteId($result['transactionid']);
    $payment->setAuthorizedTime($request_time);

    if ($capture) {
      $payment->setCompletedTime($request_time);
    }

    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    // Perform the capture request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

     try {
      // Try a capture api call.
      $result = $this->payKingsClient->doRequestByType(
        'capture',
        $number,
        [],
        '',
        $remote_id
      );

      if ($result['responsetext'] !== 'SUCCESS' && $result['response'] != 1) {
        $errorMessage = $result['responsetext'] . ': ' . $result['responsetext'];

        // Log and throw the error.
        $this->getLogger('commerce_paykings')->error($errorMessage);
        throw new PayKingsClientException('
          The Capture payment for Paykings has been declined!'
        );
      }
      else {
        $payment->setState('completed');
        $payment->setAmount($amount);
        $payment->save();
      }
    }
    catch(PayKingsClientException $e) {
      $this->getLogger('commerce_paykings')->warning($e->getMessage());
      $this->messenger()->addWarning($this->t(
        'The Void Api Call could not be processed'
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // Perform the void request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();

     try {
      // Try a void api call.
      $result = $this->payKingsClient->doRequestByType(
        'void',
        0,
        [],
        '',
        $remote_id
      );

      if ($result['responsetext'] !== 'SUCCESS' && $result['response'] != 1) {
        $errorMessage = $result['responsetext'] . ': ' . $result['responsetext'];

        // Log and throw the error.
        $this->getLogger('commerce_paykings')->error($errorMessage);
        throw new PayKingsClientException('
          The Void payment for Paykings has been declined!'
        );
      }
      else {
        $payment->setState('authorization_voided');
        $payment->save();
      }
    }
    catch(PayKingsClientException $e) {
      $this->getLogger('commerce_paykings')->warning($e->getMessage());
      $this->messenger()->addWarning($this->t(
        'The Void Api Call could not be processed'
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    try {
      // Try a refund api call.
      $result = $this->payKingsClient->doRequestByType(
        'refund',
        $number,
        [],
        '',
        $remote_id
      );

      if ($result['responsetext'] !== 'SUCCESS' && $result['response'] != 1) {
        $errorMessage = $result['responsetext'] . ': ' . $result['responsetext'];

        // Log and throw the error.
        $this->getLogger('commerce_paykings')->error($errorMessage);
        throw new PayKingsClientException('
          The provided payment for Paykings has been declined!'
        );
      }
      else {
        $old_refunded_amount = $payment->getRefundedAmount();
        $new_refunded_amount = $old_refunded_amount->add($amount);

        if ($new_refunded_amount->lessThan($payment->getAmount())) {
          $payment->setState('partially_refunded');
        }
        else {
          $payment->setState('refunded');
        }

        $payment->setRefundedAmount($new_refunded_amount);
        $payment->save();
      }
    }
    catch(PayKingsClientException $e) {
      $this->getLogger('commerce_paykings')->warning($e->getMessage());
      $this->messenger()->addWarning($this->t(
        'The refund could not be processed'
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(
    PaymentMethodInterface $payment_method,
    array $payment_details
  ) {
    $this->state->set('payment_details', $payment_details);
    $payment_method->card_type = $payment_details['type'];
    // Only the last 4 numbers are safe to store.
    $payment_method->card_number = substr($payment_details['number'], -4);
    $payment_method->card_exp_month = $payment_details['expiration']['month'];
    $payment_method->card_exp_year = $payment_details['expiration']['year'];
    $expires = CreditCard::calculateExpirationTimestamp(
      $payment_details['expiration']['month'],
      $payment_details['expiration']['year']
    );

    $payment_method->setReusable(FALSE);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * Delete the payment instance to fix the list of payment methods.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The current instance of payment.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The current order.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \InvalidArgumentException
   */
  public function deletePayment(
    PaymentInterface $payment,
    OrderInterface $order
  ) {

    $payment->delete();
    $order->set('payment_method', NULL);
    $order->set('payment_gateway', NULL);
    $order->save();
  }

}
