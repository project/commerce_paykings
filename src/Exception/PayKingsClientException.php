<?php

namespace Drupal\commerce_paykings\Exception;

/**
 * Class PayKingsClientException.
 *
 * @package Drupal\commerce_paykings\Exception
 */
class PayKingsClientException extends \Exception {

}
