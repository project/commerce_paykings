<?php

namespace Drupal\commerce_paykings\Client;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Pay Kings Client interface.
 */
interface PayKingsClientInterface {

  /**
   * Execute the payment request to paykigns.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param array $configuration
   *   The payment method configuration.
   *
   * @throws \Drupal\commerce_paykigns\Exception\PaykingsClientException
   */
  public function doRequest(PaymentInterface $payment, array $configuration);

  /**
   * Get the secret Key.
   *
   * @param array $configuration
   *   The plugin configuration.
   *
   * @return string
   *   The secret key.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function getSecretKey($configuration);

  /**
   * Set the payment details to the class object.
   *
   * @param string $type
   *   The data type.
   * @param array $data
   *   The array of data.
   */
  public function setClientData(string $type, array $data);

  /**
   * Make the requests by API request type.
   *
   * @param string $requestType
   *   The request type.
   * @param float $amount
   *   The order amount.
   * @param array $cardDetails
   *   The card details array.
   * @param string $code
   *   The authorization code string.
   * @param string $id
   *   The transaction id string.
   */
  public function doRequestByType(
    string $requestType,
    float $amount = 0,
    array $cardDetails,
    string $code = '',
    string $id = ''
  );

  /**
   * Add Transaction Id to the query.
   *
   * @param string $id
   *   The transaction id string.
   */
  public function addTransactionId(string $id);

  /**
   * Add Authorization Code to the query.
   *
   * @param string $code
   *   The Authorization Code string.
   */
  public function addAuthCode(string $code);

  /**
   * Add Order Amount to the query.
   *
   * @param float $amount
   *   The Order Amount.
   */
  public function addAmount(float $amount);

  /**
   * Builds the POST query details.
   *
   * @param string $type
   *   The request post type.
   * @param array $details
   *   The array of details.
   *
   * @return string
   *   Return a string with query params;
   */
  public function buildQuery(string $type, array $details = []);

  /**
   * Cleans the POST query.
   *
   * @param string $query
   *   The request post type.
   *
   * @return string
   *   Return a string with query params;
   */
  public function cleanQuery(string $query);

  /**
   * Make CURL request with params.
   *
   * @param string $query
   *   The request post type.
   *
   * @return array
   *   Return post response;
   */
  public function doPostRequest(string $query);

  /**
   * Gets the shipping profile, if exists.
   *
   * The function safely checks for the existence of the 'shipments' field,
   * which is installed by commerce_shipping. If the field does not exist or is
   * empty, NULL will be returned.
   *
   * The shipping profile is assumed to be the same for all shipments.
   * Therefore, it is taken from the first found shipment, or created from
   * scratch if no shipments were found.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   *   The shipping profile.
   */
  public function getShippingProfile(OrderInterface $order);

  /**
   * Configuring the Details for the Request.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   * @param array $configuration
   *   The order entity.
   */
  public function setRequestDetails(OrderInterface $order, array $configuration);

}
