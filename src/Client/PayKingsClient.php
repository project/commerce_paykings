<?php

namespace Drupal\commerce_paykings\Client;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_paykings\Client\PayKingsClientInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_paykings\Exception\PayKingsClientException;
use Drupal\Core\State\State;
use Drupal\Core\TypedData\Exception\MissingDataException;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class PayKingsClient.
 *
 * @package Drupal\commerce_paykings\Client
 */
class PayKingsClient implements PayKingsClientInterface {

  /**
   * The Drupal State Service.
   *
   * @var \Drupal\Core\State\State
   */
  private $state;

  /**
   * Constructs a new PayKingsClient object.
   *
   * @param \Drupal\Core\State\State $payKingsClient
   *   The Drupal State Service.
   */
  public function __construct(State $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * The API Url.
   */
  const API_URL = 'https://paykings.transactiongateway.com/api/transact.php';

  /**
   * The API test key.
   */
  const TEST_KEY = '6457Thfj624V5r7WUwc5v6a68Zsd6YEm';

  /**
   * {@inheritdoc}
   */
  public function doRequest(PaymentInterface $payment, array $configuration) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    // Set request details.
    $this->setRequestDetails($order, $configuration);

    try {
      $paymentDetails = $this->state->get('payment_details');

      if (empty($paymentDetails)) {
        throw new PayKingsClientException('Request failed due to missing data');
      }

      $cardData = [
        'ccnumber' => $paymentDetails['number'],
        'ccexp' => $paymentDetails['expiration']['month'] . substr($paymentDetails['expiration']['year'], -2),
        'cvv' => $paymentDetails['security_code'],
      ];

      $this->state->delete('payment_details');

      return $this->doRequestByType(
        'sale',
        $order->getTotalPrice()->getNumber(),
        $cardData
      );
    }
    catch (InvalidArgumentException $e) {
      throw new PayKingsClientException('Request failed due to invalid user.');
    }
    catch (MissingDataException $e) {
      throw new PayKingsClientException('Request failed due to missing data.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSecretKey($configuration) {
    switch ($configuration['mode']) {
      case 'test':
        $secretKey = static::TEST_KEY;
        break;

      case 'live':
        $secretKey = $configuration['secret_key'];
        break;

      default:
        throw new MissingDataException('The Secret Key is empty.');
    }

    return $secretKey;
  }

  /**
   * {@inheritdoc}
   */
  public function setClientData(string $type, array $data) {
    foreach ($data as $key => $info) {
      $this->{$type}[$key] = $info;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function doRequestByType(
    string $requestType,
    float $amount = 0,
    array $cardDetails,
    string $code = '',
    string $id = ''
    ) {

    $query = '';

    // Add requestType to the query.
    $query .= "type=" . $requestType . "&";

    // Add login key to the query.
    $query .= $this->buildQuery('login');

    // Add transactionid only for $requestType ('capture', 'void', 'refund').
    if ($requestType === 'capture' || $requestType === 'void' || $requestType === 'refund') {
      $query .= $this->addTransactionId($id);
    }

     // Return query for the $requestType ('void).
    if ($requestType === 'void') {
      return $this->doPostRequest($query);
    }

    // Add amount to the query.
    if ($amount > 0) {
      $query .= $this->addAmount($amount);
    }

    // Return query for the $requestType ('capture', refund').
    if ($requestType === 'capture' || $requestType === 'refund') {
      return $this->doPostRequest($query);
    }

    // For the $requestType ('credit', 'offline') no CVV is required.
    if (($requestType === 'credit' || $requestType === 'offline') && isset($cardDetails['cvv'])) {
      unset($cardDetails['cvv']);
    }

    // Add card details to the query.
    $query .= $this->buildQuery('card', $cardDetails);

    // Add authorization code to the query, only for $requestType ('offline').
    if ($requestType === 'offline') {
      $query .= $this->addAuthCode($code);
    }

    // Add order & billing info to the query.
    $query .= $this->buildQuery('order');
    $query .= $this->buildQuery('billing');

    // Add order & billing info to the query. except ('credit') $requestType.
    if ($requestType !== 'credit') {
      $query .= $this->buildQuery('shipping');
    }

    return $this->doPostRequest($query);
  }

  /**
   * {@inheritdoc}
   */
  public function addTransactionId(string $id) {
    return "transactionid=" . urlencode($id) . "&";
  }

  /**
   * {@inheritdoc}
   */
  public function addAuthCode(string $code) {
    return "authorizationcode=" . urlencode($code) . "&";
  }

  /**
   * {@inheritdoc}
   */
  public function addAmount(float $amount) {
    return "amount=" . urlencode(number_format($amount, 2, ".", "")) . "&";
  }

  /**
   * {@inheritdoc}
   */
  public function buildQuery(string $type, array $details = []) {
    $queryParam = '';

    // Get details from the class proprietes if not passed.
    if (empty($details)) {
      $details = $this->{$type};
    }

    foreach ($details as $key => $data) {
      // Add special key adjustements.
      if ($type === 'login') {
        $key = 'security_key';
      }
      elseif ($type === 'shipping') {
        $key = 'shipping_' . $key;
      }

      // Format the numbers.
      if ($key === 'tax' || $key === 'shipping') {
        $data = number_format($data, 2, ".", "");
      }

      $queryParam .= $key . "=" . urlencode($data) . "&";
    }

    return $queryParam;
  }

  /**
   * {@inheritdoc}
   */
  public function cleanQuery(string $query) {
    return rtrim($query, '&') . '';
  }

  /**
   * {@inheritdoc}
   */
  public function doPostRequest($query) {
    // Clean Query, removes "&" if this is the last character.
    $query = $this->cleanQuery($query);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, static::API_URL);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    // Not recommended, may be required, uncomment if yes.
    // TODO: curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);.
    curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
    curl_setopt($ch, CURLOPT_POST, 1);

    if (!($data = curl_exec($ch))) {
      return NULL;
    }

    curl_close($ch);
    unset($ch);

    $data = explode("&", $data);

    foreach ($data as $info) {
      $rdata = explode("=", $info);
      $this->responses[$rdata[0]] = $rdata[1];
    }

    return $this->responses;
  }

  /**
   * {@inheritdoc}
   */
  public function getShippingProfile(OrderInterface $order) {
    if ($order->hasField('shipments')) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      foreach ($order->shipments->referencedEntities() as $shipment) {
        return $shipment->getShippingProfile();
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setRequestDetails(OrderInterface $order, array $configuration) {
    $secretKey = $this->getSecretKey($configuration);

    // Set Security Key.
    $this->setClientData('login', ['security_key' => $secretKey]);

    $billing_profile = $order->getBillingProfile();
    $billing = $billing_profile
      && $billing_profile->hasField('address')
      && !$billing_profile->get('address')->isEmpty() ?
      $billing_profile->address->first() : NULL;

    // Set Billing details.
    $this->setClientData(
      'billing',
      [
        'firstname' => $billing ? $billing->getGivenName() : '',
        'lastname' => $billing ? $billing->getFamilyName() : '',
        'company' => $billing ? $billing->getOrganization() : '',
        'address1' => $billing ? $billing->getAddressLine1() : '',
        'address2' => $billing ? $billing->getAddressLine2() : '',
        'city' => $billing ? $billing->getLocality() : '',
        'state' => $billing ? $billing->getAdministrativeArea() : '',
        'zip' => $billing ? $billing->getPostalCode() : '',
        'country' => $billing ? $billing->getCountryCode() : '',
        'email' => $order->getEmail(),

        // The commerce does not requires the following values.
        'phone' => '',
        'fax' => '',
        'website' => '',
      ]
    );

    $shipping_profile = $this->getShippingProfile($order);
    $shipping = $shipping_profile
      && $shipping_profile->hasField('address')
      && !$shipping_profile->get('address')->isEmpty() ?
      $shipping_profile->address->first() : NULL;

    // Set Shipping details.
    $this->setClientData(
      'shipping',
      [
        'firstname' => $shipping ? $shipping->getGivenName() : '',
        'lastname' => $shipping ? $shipping->getFamilyName() : '',
        'company' => $shipping ? $shipping->getOrganization() : '',
        'address1' => $shipping ? $shipping->getAddressLine1() : '',
        'address2' => $shipping ? $shipping->getAddressLine2() : '',
        'city' => $shipping ? $shipping->getLocality() : '',
        'state' => $shipping ? $shipping->getAdministrativeArea() : '',
        'zip' => $shipping ? $shipping->getPostalCode() : '',
        'country' => $shipping ? $shipping->getCountryCode() : '',
        'email' => $order->getEmail(),
      ]
    );

    // Set Order details.
    $this->setClientData(
      'order',
      [
        'orderid' => $order->id(),
        'orderdescription' => 'Order: ' . $order->id(),
        'ipaddress' => $order->getIpAddress(),
        // Drupal Commerce does not requires PO Number.
        'ponumber' => '',
        // Currently Order does not include the TAX & Shipping price
        // calculation, those will be added later.
        'tax' => 0,
        'shipping' => 0,
      ]
    );
  }

}
