<?php

namespace Drupal\commerce_paykings\Client;

use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Pay Way Client interface.
 */
interface PayKingsRestApiClientInterface {

  /**
   * Execute the payment request to paykigns.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   * @param array $configuration
   *   The payment method configuration.
   *
   * @throws \Drupal\commerce_paykigns\Exception\PaykingsClientException
   */
  public function doRequest(PaymentInterface $payment, array $configuration);

  /**
   * Get client response.
   *
   * @return string
   *   Body of the client response.
   */
  public function getResponse();

}
