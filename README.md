INTRODUCTION
------------

# Commerce Paykings
Commerce Paykings gateway integrates the [Paykings](https://www.paykings.com/)
gateway for the [Drupal Commerce](https://www.drupal.org/project/commerce).

REQUIREMENTS
------------
[Drupal Commerce](https://www.drupal.org/project/commerce).

RECOMMENDED MODULES
-------------------
No modules recommended.

INSTALLATION
------------
Install as usual Drupal module via Admin UI or Composer.

CONFIGURATION
-------------
1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Commerce > Configuration > Payment > Payment
Gateways. Add payment Gateway, choose Paykings and configure it using
information from you Paykings account , get one on
[Paykings website](https://www.paykings.com/).
3. Now, on the checkout if user will select Paykings payment, the payment
process will go through Paykings Api.

TROUBLESHOOTING
---------------
Feel Free to open an issue or find the solution for the existing one on the
[issues list](https://www.drupal.org/project/issues/paykings).

MAINTAINERS
-----------
  * Lilian Catanoi (liliancatanoi90) - https://www.drupal.org/u/liliancatanoi90

This project is sponsored by:
 * [OPTASY](https://www.optasy.com) is a Canadian Drupal development & web
  development company based in Toronto. In the past we provided Drupal
  development solutions for a variety of Canadian and foregin companies with
  outstanding results.
